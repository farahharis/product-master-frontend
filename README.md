# product-master-frontend

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Prerequisites

- Node v20.11.1
- NPM v10.2.4
- IDE
- Vue3

## Project Setup

### Clone project

```sh
git clone https://gitlab.com/farahharis/product-master-frontend.git
```

### Change directory to product-master-frontend

```sh
cd product-master-frontend
```

### Setup environment variables

```sh
cp .env.example .env.local
```

Replace VITE_APP_API_URL with appropriate value

### Install packages during fresh start

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

Ensure that VITE_APP_API_URL in .env file is pointing to correct host. Usually it's `http://127.0.0.1:8000`

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Available functionalities

1. List products with pagination
2. Search for products based on product ID or capacity or types or brand or model name
3. Bulk upload products
4. WIP: Sort products based on a column
