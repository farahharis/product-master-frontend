// src/composables/useApi.js
import axios from 'axios';
import { toast } from 'vue3-toastify';

export default function useAxiosApi() {
    const apiUrl = import.meta.env.VITE_APP_API_URL;

    // Upload File
    const uploadFile = async (file, urlEndpoint) => {
        try {
            const formData = new FormData();
            formData.append('file', file);

            const response = await axios.post(`${apiUrl}${urlEndpoint}`, formData);
            console.log('toasting........');
            toast.success(response.data.message);

            return { success: true, data: response.data };
        } catch (error) {
            console.error(JSON.stringify(error.response.data));
            toast.error(error.response.data.message, { autoClose: 2000 });
            return { success: false, error: error.response.data };
        }
    };

    // GET Records
    const fetchRecords = async (urlEndpoint, params = {}) => {
        try {
            const response = await axios.get(`${apiUrl}${urlEndpoint}`, { params });
            return { success: true, data: response.data };
        } catch (error) {
            console.error(JSON.stringify(error.response.data));
            toast.error("Failed to fetch records.", { autoClose: 2000 });
            return { success: false, error: error.response.data };
        }
    };

    return { uploadFile, fetchRecords };
}
